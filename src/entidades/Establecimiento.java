/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.*;
import interfaz.*;
import java.time.*;

/**Declaracion de la clase Establecimiento
 *
 * @author Jose
 * @version 09/07/2019
 *
 */
public class Establecimiento {
    //Declaracion de atributos privados.
    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String ciudad;
    private Sector sector;
    private String direccion;
    private String[] telefonos;
    private LocalTime[] horario;
    private Empresa empresa;
    private ArrayList<Promocion> promociones;
    
    /** Creacion del constructor.
     * 
     * @param ciudad Indica la Ciudad donde se encuentra el establecimiento.
     * @param sector Indica el Sector donde se encuentra el establecimiento.
     * @param direccion Indica la Direccion del establecimiento.
     * @param telefonos Indica los telefonos de establecimiento.
     * @param horario Indica los horarios del establecimientos
     * @param empresa Indica la Empresa a la que pertenece el establecimiento
     */
    public Establecimiento(String ciudad, Sector sector, String direccion, String[] telefonos, LocalTime[] horario, Empresa empresa) {
        this.codigo = codigoIncremental++;
        this.ciudad = ciudad;
        this.sector = sector;
        this.direccion = direccion;
        this.telefonos = telefonos;
        this.horario = horario;
        this.empresa = empresa;
        this.promociones = new ArrayList<>();
    }
    /** Metodo comprobarEstablecimiento() que busca y muestra un establecimiento de una empresa a travez de su direccion.
     * 
     * @param empresa Indica la Empresa a la que pertence el establecimiento.
     * @param direccion Indica la direccion del establecimiento.
     * @return 
     */
    public static Establecimiento comprobarEstablecimiento(Empresa empresa, String direccion) {
        Establecimiento establecimiento = null;
        for (Establecimiento est : empresa.getEstablecimientos()){
            if (est.getDireccion().equalsIgnoreCase(direccion)){
                establecimiento = est;
                break;
            }
        }                
        return establecimiento;
    }
    /** Metodo registrarEstablecimiento que registra un establecimiento en una empresa del sistema.
     * 
     * @param empresa Indica la empresa a la que pertenece el establecimiento.
     */
    public static void registrarEstablecimiento(Empresa empresa) {
        System.out.print("Ingrese direccion del establecimiento : ");
        String nDireccion = Util.ingresoString();
        if (Establecimiento.comprobarEstablecimiento(empresa, nDireccion) == null) {
            System.out.print("Ingrese ciudad : ");
            String nCiudad = Util.ingresoString();            
            for (Sector s : Sector.values()){
                System.out.println((s.ordinal()+1) + ") " + s);
            }
            System.out.print("Ingrese el numero correspondiente al sector : ");
            String n = Util.ingresoString();                                       
            while ( (!(Util.isNumeric(n))) || (!(Util.isBetween(1, 5, n))) ) {
                System.out.print("Opci�n incorrecta. Ingrese nuevamente : ");
                n = Util.ingresoString();                                
            }
            Integer N = Integer.parseInt(n);
            Sector nSector = Sector.values()[N-1];            
            System.out.print("Ingrese tel�fonos separados por comas : ");
            String[] nTelefonos = Util.ingresoString().split(",");
            System.out.print("Ingrese horario de apertura (HH:MM) : ");
            LocalTime nHorarioA = Util.ingresoHora();
            while (nHorarioA == null){
                System.out.print("Formato inv�lido. Ingrese la hora correctamente (HH:MM) : ");
                nHorarioA = Util.ingresoHora();
            }
            System.out.print("Ingrese horario de cierre (HH:MM) : ");
            LocalTime nHorarioC = Util.ingresoHora();
            while (nHorarioC == null){
                System.out.print("Formato inv�lido. Ingrese la hora correctamente (HH:MM) : ");
                nHorarioC = Util.ingresoHora();
            }
            LocalTime[] nHorario = new LocalTime[] {nHorarioA, nHorarioC};
            Establecimiento nuevoEstablecimiento = new Establecimiento(nCiudad, nSector, nDireccion, nTelefonos, nHorario, empresa);
            empresa.getEstablecimientos().add(nuevoEstablecimiento);            
            System.out.println();
            System.out.println("Establecimiento agregado con �xito");
            Util.continuar();
        } else {
            System.out.println();
            System.out.println("El establecimiento ya existe");
            Util.continuar();
        }
    }

    /* GETTERS & SETTERS */
    public Integer getCodigo() {
        return codigo;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String[] getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String[] telefonos) {
        this.telefonos = telefonos;
    }

    public LocalTime[] getHorarios() {
        return horario;
    }

    public void setHorarios(LocalTime[] horarios) {
        this.horario = horarios;
    }

    public ArrayList<Promocion> getPromociones() {
        return promociones;
    }

    @Override
    public String toString() {
        return "Ciudad : " + ciudad + "\n" + "Sector : " + sector + "\n" + "Direcci�n : " + direccion + "\n" + "Horario de atenci�n : " 
                + Arrays.asList(horario).toString() + "\n" + "Tel�fonos : " + Arrays.asList(telefonos).toString();
    }


    

    
}

